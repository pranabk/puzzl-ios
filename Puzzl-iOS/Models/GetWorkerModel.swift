//
//  GetWorkerModel.swift
//  Puzzl-iOS
//
//  Created by Artem Dudinski on 4/29/20.
//  Copyright © 2020 Denis Butyletskiy. All rights reserved.
//

import Foundation

class GetWorkerModel: Codable {
    
    var createdAt: String
    var defaultOtWage: Int
    var defaultWage: Int
    var email: String
    var firstName: String
    var lastName: String
    var title: String
    
    private enum CodingKeys: String, CodingKey {
        case createdAt
        case defaultOtWage = "default_ot_wage"
        case defaultWage = "default_wage"
        case email
        case firstName = "first_name"
        case lastName = "last_name"
        case title
    }
}
