//
//  SignW2Model.swift
//  Puzzl-iOS
//
//  Created by Artem Dudinski on 4/30/20.
//  Copyright © 2020 Denis Butyletskiy. All rights reserved.
//

import Foundation

class SignW2Model: Codable {
    var companyID: String
    var firstName: String
    var lastName: String
    var middlInitial: String?
    var title: String
    var address: String
    var city: String
    var state: String
    var zip: Int
    var ssn: String
    var email: String
    var defaultWage: Int
    var defaultOtWage: Int
    //var phoneNumber: String
    var createdAt: String
    
    private enum CodingKeys: String, CodingKey {
        case createdAt
        case defaultOtWage = "default_ot_wage"
        case defaultWage = "default_wage"
        case email
        case firstName = "first_name"
        case lastName = "last_name"
        case title
        case companyID
        case middlInitial = "middle_initial"
        case address
        case city
        case state
        case zip
        case ssn
        //case phoneNumber = "phone_number"
    }
    
    init() {
        companyID = "\(Puzzl.companyID)"
        firstName = ""
        lastName = ""
        middlInitial = ""
        title = ""
        address = ""
        city = ""
        state = ""
        state = ""
        zip = 0
        ssn = ""
        email = ""
        defaultWage = 0
        defaultOtWage = 0
        //phoneNumber = ""
        createdAt = ""
    }
}
